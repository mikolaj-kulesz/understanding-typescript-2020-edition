abstract class Department {

    public readonly name: string // add public or leave since public is default, readonly will ensure that this property will be overwritten after initialization
    protected employees: string[] = []

    protected constructor(n: string) {
        this.name = n
    }

    // shorter version
    // constructor(public name: string) {
    //     //this.name = n
    // }

    abstract get additionalInfo(): string

    describe(this: Department) {
        console.log('Describe: ' + this.name)
    }

    add(employee: string) {
        // this.name = 'asdas' - since name is readonly, you can not overwrite it
        this.employees.push(employee)
    }

    printEmployees() {
        console.log(this.employees.length)
        console.log(this.employees)
    }
}

// const department = new Department('Accounting')
// console.log('DEPARTMENT:', department)

// department.describe()

// department.employees[4] = 'vvv' //can't access private properties from outside the class

// department.add('sadasd')
// department.printEmployees()

// const newDepartment = {
//     add: () => {},
//     printEmployees: () => {},
//     employees: [],
//     name: "BLABLA",
//     describe: department.describe
// }
//
// newDepartment.describe()

// > newDepartment.describe()
// we will get "Describe: undefined" here since newDepartment.describe is a pointer to method "describe", so this part is correct
// but this method use's this.name, at this is being searched in newDepartment, which do not contain this data, such property

class ItDepartment extends Department {
    private admins: string[]

    constructor(admins: string[]) {
        super('IT',)
        this.admins = admins;
    }

    get additionalInfo(): string {
        return 'This is IT Department!'
    }
}

const itDepartment = new ItDepartment(['MINI_MAX, ODYN'])
console.log('itDepartment', itDepartment)


// =========================================

class AccountDepartment extends Department {
    static fiscalYear = 2020
    private reports: string[] = []
    private lastReport: string = this.reports[0]

    constructor() {
        super('ACCOUNT')
    }

    get mostRecentReport() {
        if (this.lastReport) {
            return this.lastReport
        }
        console.log('No last report was found!');
        return ''
    }

    set mostRecentReport(reportName: string) {
        if (!reportName) {
            throw new Error('Add valid report!')
        }
        this.addReport(reportName);
    }

    get additionalInfo(): string {
        return 'This is IT Department!'
    }

    static someCustomLog(value: any) {
        AccountDepartment.fiscalYear++
        console.log('--->', value)
    }

    addReport(reportName: string) {
        this.reports.push(reportName)
        this.lastReport = reportName;
    }

    printReports() {
        console.log('reports', this.reports)
    }

    add(employee: string) {
        this.employees.push(employee.toUpperCase())
    }

}


const accountDepartment = new AccountDepartment()
console.log('accountDepartment', accountDepartment)
console.log('mostRecentReport', accountDepartment.mostRecentReport)
accountDepartment.addReport('AXV_675')
accountDepartment.addReport('AZZ_123')
console.log('mostRecentReport', accountDepartment.mostRecentReport)
console.log('===========================================')
accountDepartment.mostRecentReport = 'Report 2020!'
console.log('mostRecentReport', accountDepartment.mostRecentReport)
console.log('===========================================')
accountDepartment.add('Anna')
accountDepartment.add('Mike')
accountDepartment.printReports()
accountDepartment.printEmployees()
console.log('===>', accountDepartment.additionalInfo);

// call static method from the class itself
console.log(AccountDepartment.fiscalYear)
AccountDepartment.someCustomLog('XXX')
AccountDepartment.someCustomLog(AccountDepartment.fiscalYear)
AccountDepartment.someCustomLog(AccountDepartment.fiscalYear)

// ===============================================
// SINGLETON EXAMPLE
// ===============================================
class SnowFlake {
    private static instance: SnowFlake
    readonly size: number;
    private name: string;

    private constructor(n: string, s: number) {
        this.name = n;
        this.size = s;
    }

    get myName() {
        return this.name;
    }

    set myName(name) {
        this.name = name;
    }

    get mySize() {
        return this.size;
    }

    static getInstance() {
        if (this.instance) {
            return this.instance
        }
        this.instance = new SnowFlake('Miriam', 20)
        return this.instance;
    }

    public log() {
        console.log(`I'm a snowflake called ${this.name}. My size id ${this.size}`)
    }
}

const snowFlake = SnowFlake.getInstance()
const snowFlake2 = SnowFlake.getInstance()
console.log('snowFlake', snowFlake)
console.log('snowFlake2', snowFlake2)
console.log('test', snowFlake === snowFlake2)
