// Intersection types

type Admin = {
    name: string
    privileges: string[]
}

type Employee = {
    name: string,
    date: Date,
}

type SuperEmployee = Admin & Employee

const someEmployee: SuperEmployee = {
    date: new Date(),
    name: 'MIKE',
    privileges: ['ADMIN', 'EMPLOYEE']
}


// the same goes for interfaces and extend

interface Admin2 {
    name: string
    privileges: string[]
}

interface Employee2 {
    name: string,
    date: Date,
}

interface SuperEmployee2 extends Admin2, Employee2 {
}

const someEmployee2: SuperEmployee2 = {
    date: new Date(),
    name: 'MIKE',
    privileges: ['ADMIN', 'EMPLOYEE'],
}

// one more example

type typeA = number | string
type typeB = number | boolean
type typeC = typeA & typeB

const someC: typeC = 4

// Type Guards

// Example: 1 - typeof
function myCustomAdd(a: typeA, b: typeA) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}

// Example: 2 - typeof
type SuperEmployee3 = Admin | Employee

const se3: SuperEmployee3 = {
    name: "MAJK3",
    privileges: ["ABC"],
}

const se4: SuperEmployee3 = {
    name: "MAJK4",
    date: new Date(),
}

function printEmployeeInfo(e: SuperEmployee3) {
    console.log(e.name);
    if ('privileges' in e) {
        console.log(e.privileges);
    }
    if ('date' in e) {
        console.log(e.date);
    }
}

printEmployeeInfo(se3)
printEmployeeInfo(se4)


// Example: 3 - typeof or instance of when Class
class Car {
    drive() {
        console.log('driving...')
    }
}

class Track {
    drive() {
        console.log('driving a track...')
    }

    loadCargo(n: number) {
        console.log(`loading ${n} tons of stuff...`)
    }
}

type Vehicle = Car | Track

const v1 = new Car()
const v2 = new Track()

function useVehicle(v: Vehicle) {
    v.drive()
    if ('loadCargo' in v) {
        v.loadCargo(5)
    }
    if (v instanceof Track) {
        v.loadCargo(5)
    }
}

useVehicle(v1)
useVehicle(v2)
