// Interfaces

interface Human {
    name: string,
    age: number,
    hobbies?: string[],
    favouriteColor?: string

    sayHello(value: string): void
}

const rob: Human = {
    age: 22,
    hobbies: ['skiing', 'dancing'],
    name: 'Rob',
    sayHello(someName: string) {
        console.log(`Hello, I'm ${this.age}.' How old is you ${someName}?`)
    }
}

console.log('Rob', rob);
rob.sayHello('Tim');

// =====================================================================
interface Human2 {
    name: string,
    readonly age: number | null,
    hobbies: string[],
}

interface Human3 extends Human2 {
    favouriteColor?: string | null

    sayHello?(value: string): void
}

class NewPerson implements Human3 {
    name: string;
    age: number | null;
    hobbies: string[] = [];
    favouriteColor?: string;

    constructor(human: Human, favouriteColor?: string) {
        const {
            name,
            age,
        } = human
        this.name = name;
        this.age = age
        if (favouriteColor) this.favouriteColor = favouriteColor
    }

    sayHello(someName: string) {
        console.log(`Hi, my name is ${this.name}, I'm ${this.age} years old.' How old is you ${someName}?`)
    }

}

const newPerson: Human3 = new NewPerson(rob, 'pink');
console.log('newPerson', newPerson)
if (newPerson.sayHello) newPerson.sayHello('Tim');
// newPerson.age = 24; // this will not be possible since age is readonly in our Human2 interface


// Interface as Function Types
type AddFn = (a: number, b: number) => number
const myAdd: AddFn = (a: number, b: number): number => a + b
console.log(myAdd(3, 4));

// or
interface Add2Fn {
    (a: number, b: number): number
}

const myAdd2: Add2Fn = (a: number, b: number): number => a + b
