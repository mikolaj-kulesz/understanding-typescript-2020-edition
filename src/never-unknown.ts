let userInput: unknown
let userName: string

userInput = 'Max'
userInput = 5

if (typeof userInput === 'string') {
    userName = userInput;
}

// if we use "any" instead of "unknown", this will work
// unknown is better then any since it at least require some basic type checking
// any disables all types checking

// =======================================

const generateError = (errorName: string, code: number): never => {
    throw {
        name: errorName,
        code
    }
    // this function is intended to never return a value,
    // code will always crush in here
    // could be tested if we console log the value of this function - it does not produce the undefined
}

// while (true) {} will also return a "never"
console.log('=================')

generateError('Custom Error', 999);

generateError('Custom Error', 999);

console.log(generateError('Custom Error', 999));

console.log(generateError('Custom Error', 999));
