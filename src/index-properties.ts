// Index properties

interface ErrorContainer {
    [prop: string]: string
}

const errorExample1: ErrorContainer = {} // empty object will work here since properties are not required

const errorExample2: ErrorContainer = { // now you can add whatever properties you like, but hey need to be strings
    name: 'Example2',
    type: 'strongError',
};

const errorExample3: ErrorContainer = {
    status: 'done',
    type: 'strongError567'
};
