const add = (a: number, b: number): number => {
    return a + b;
};

const printValue = (value: number = 2, label: string = 'Default label', status: boolean = true): void => {
    if (status) {
        console.log(`${label}: ${value}`);
        return
    }
    console.log(`${value}`);
}

const number1 = 5;
const number2 = 5;

const res = add(number1, number2);

printValue();
printValue(res);
printValue(res, 'myLabel');
printValue(res, 'myLabel', false);
