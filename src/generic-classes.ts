class DataStorage<T extends string | number | boolean> {
    private data: T[] = [];

    addItem(item: T){
        this.data.push(item);
    }

    removeItem(item: T){
        if(this.data.indexOf(item) === -1) return;
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems(){
        return this.data;
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Max');
textStorage.addItem('Manu');
textStorage.removeItem('Max');
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number>();
numberStorage.addItem(1);
numberStorage.addItem(5);
numberStorage.removeItem(7);
console.log(numberStorage.getItems());

const booleanStorage = new DataStorage<boolean>();
booleanStorage.addItem(true);
booleanStorage.addItem(true);
booleanStorage.removeItem(true);
console.log(booleanStorage.getItems());

// Generic Utility types
interface CourseGoal {
    title: string,
    description: string,
    completeUntil: Date,
}

const createCourseGoal = (t: string, d: string, cu: Date): CourseGoal => {
    const obj: Partial<CourseGoal> = {};
    obj.title = t;
    obj.description = d;
    obj.completeUntil = cu;
    return <CourseGoal>obj
};

const namesArr : Readonly<string[]> = ['Max', 'Miki'];
// namesArr.push('Adam'); // now this is not allowed