const addMany = (...params: number[]): number => {
    return params.reduce((total, currentItem) => {
        return total + currentItem
    }, 0)
}

const test1 = addMany(1, 2, 3)
const test2 = addMany(1, 2, 3, 6, 7, 8, 9)
const test3 = addMany()

console.log('test1', test1)
console.log('test2', test2)
console.log('test3', test3)

const fruits = ['bananas', 'apples', 'oranges']
const [fruitOne, ...otherFruits] = fruits
console.log(fruitOne, otherFruits);

const somePerson = {
    someName: 'mike',
    age: 22,
}
const {someName: someCustomNameKey, age} = somePerson
console.log(someCustomNameKey, age)

// const {customName: name} = somePerson
// console.log(customName, age)
