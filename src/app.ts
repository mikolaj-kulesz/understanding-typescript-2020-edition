// @Decorators
function Logger(constructor: Function) {
    console.log('Logger', constructor)
}

@Logger
class HumanBeing {
    private name: string = 'Mikol';

    constructor() {
        console.log('HumanBeing is loading...')
    }
}

const humanBeing = new HumanBeing();
console.log(humanBeing);

// @Decorators2 - factory approach
// now we remove a decorator function
console.log('====================================================');

function Logger2(loggerLabel: string) {
    console.log('Logger2');
    return (constructor: Function) => {
        console.log('--> Logger2: returns decorator function');
        console.log(loggerLabel, constructor)
    }
}

function WithTemplate(template: string, id: string) {
    console.log('WithTemplate');
    return (constructor: any) => {
        console.log('--> WithTemplate: returns decorator function');
        const humanBeing3 = new constructor();
        const {name} = humanBeing3;
        document.addEventListener("DOMContentLoaded", () => {
            const targetEl = document.getElementById(id);
            if (targetEl) {
                targetEl.innerHTML = template;
                targetEl.querySelector('h1')!.innerHTML = name
            }
        });

    }
}

// Returning (and changing) a Class in a Class Decorator !!!!!!!!!!!!!!!!!!!
function WithTemplate2(template: string, id: string) {
    console.log('WithTemplate2');
    return <T extends { new(...args: any[]): { name: any } }>(originalConstructor: T) => {
        console.log('--> WithTemplate: returns decorator function');
        return class extends originalConstructor {
            constructor(..._: any[]) { // with _ we let TS know that we are aware tht we are not passing any args
                super();
                console.log('new constructor');
                document.addEventListener("DOMContentLoaded", () => {
                    const targetEl = document.getElementById(id);
                    if (targetEl) {
                        targetEl.innerHTML = template;
                        targetEl.querySelector('h1')!.innerHTML = this.name
                    }
                });
            }
        }
    }
}

@Logger2("LOGGING HumanBeing2")
// @WithTemplate('<h1></h1>', 'app')
@WithTemplate2('<h1></h1>', 'app')
class HumanBeing2 {
    name: string = 'Mikolaj';

    constructor() {
        console.log('HumanBeing is loading...')
    }
}

const humanBeing2 = new HumanBeing2();
console.log(humanBeing2);

console.log('====================================================');
// Property @Decorators

// Property @Decorator
function Log(target: any, propName: string) { //it will execute when class is registered, (when you add this prop to the class, since class was never instantiated)
    console.log('@ Property Decorator!');
    console.log(target, propName); // target would the the prototype in this case (could be also a constructor when instantiated)
}

// Accessor @Decorator
function Log2(target: any, propName: string, descriptor: PropertyDescriptor) { //it will execute when class is registered, (when you add this prop to the class, since class was never instantiated)
    console.log('@ Accessor Decorator!');
    console.log(target);
    console.log(propName);
    console.log(descriptor);
}

// Method @Decorator
function Log3(target: any, propName: string, descriptor: PropertyDescriptor) { //it will execute when class is registered, (when you add this prop to the class, since class was never instantiated)
    console.log('@ Method Decorator!');
    console.log(target);
    console.log(propName);
    console.log(descriptor);
}

// Parameter @Decorator
function Log4(target: any, propName: string | Symbol, position: number) { //it will execute when class is registered, (when you add this prop to the class, since class was never instantiated)
    console.log('@ Parameter Decorator!');
    console.log(target);
    console.log(propName);
    console.log(position);
}

class Product {
    @Log
    title: string;
    private _price: number;

    @Log2
    set price(val: number) {
        this._price = val;
    }

    constructor(t: string, p: number) {
        this.title = t;
        this._price = p;
    }

    @Log3
    getPriceWithTax(@Log4 tax: number) {
        return this.price * (1 + tax);
    }
}

// Example: Creating an "AutoBind" Decorator
function AutoBind(target: any, propName: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const originalMethod = descriptor.value;
    return {
        // configurable: descriptor.configurable,
        // enumerable: descriptor.enumerable,
        get() {
            return originalMethod.bind(this);
        }
    }
}

class Printer {
    message: string = "Hello Word!";

    @AutoBind
    showMessage() {
        console.log(this.message)
    }
}

document.addEventListener("DOMContentLoaded", () => {
    const printer = new Printer();
    const button = document.querySelector("button")!;
    // button.addEventListener('click', printer.showMessage.bind(printer)) //instead of this bind we add decorator
    button.addEventListener('click', printer.showMessage)
});

// Example: Validation with Decorators
interface ValidationConfig {
    [className: string]: {
        [propName: string]: string[] // ["required",....]
    }
}

const registerValidators: ValidationConfig = {};

function Required(target: any, propName: string) {
    registerValidators[target.constructor.name] = {
        ...registerValidators[target.constructor.name],
        [propName]: registerValidators[target.constructor.name] && registerValidators[target.constructor.name][propName]
            ? [...registerValidators[target.constructor.name][propName], 'required']
            : ['required']
    }
}

function PositiveNumber(target: any, propName: string) {
    registerValidators[target.constructor.name] = {
        ...registerValidators[target.constructor.name],
        [propName]: registerValidators[target.constructor.name] && registerValidators[target.constructor.name][propName]
            ? [...registerValidators[target.constructor.name][propName], 'positive']
            : ['positive']
    }
}

function validate(obj: any) {
    const validationConfigObj = registerValidators[obj.constructor.name];
    const checks: boolean[] = Object.keys(obj).map(propName => {
        const validators = validationConfigObj[propName];
        const value = obj[propName];
        const testResults: boolean[] = validators.map(validator => {
            switch (validator) {
                case('required'):
                    return !!value;
                case('positive'):
                    return value > 0;
                default:
                    return true;
            }
        });
        return !testResults.includes(false);
    });
    return !checks.includes(false);
}

class Course {
    @Required
    title: string;
    @PositiveNumber
    price: number;

    constructor(t: string, p: number) {
        this.title = t;
        this.price = p;
    }
}

document.addEventListener("DOMContentLoaded", () => {
    const form = document.querySelector('form')!;
    form.addEventListener('submit', event => {
        event.preventDefault();
        const courseTitle = (document.getElementById('title')! as HTMLInputElement).value;
        const coursePrice = +(document.getElementById('price')! as HTMLInputElement).value; // + -> to number
        const createdCourse = new Course(courseTitle, coursePrice);

        if (!validate(createdCourse)) {
            throw new Error('Say what!?');
        }
        console.log('createdCourse', createdCourse);
    });
});

// simpler version: https://www.udemy.com/course/understanding-typescript/learn/lecture/16935744#questions/8835948