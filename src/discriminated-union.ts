// discriminated union

interface Bird {
    type: 'bird'
    flyingSpeed: number
}

interface Horse {
    type: 'horse'
    runningSpeed: number
}

type Animal = Bird | Horse

function moveAnimal(a: Animal) {
    let speed
    switch (a.type) {
        case "bird":
            speed = a.flyingSpeed
            break;
        case "horse":
            speed = a.runningSpeed
            break;
    }
    console.log(`Moving at speed - ${speed} m/s`)
}

const a1: Animal = {
    type: 'horse',
    runningSpeed: 5,
}

const a2: Animal = {
    type: 'bird',
    flyingSpeed: 15,
}

moveAnimal(a1)
moveAnimal(a2)
