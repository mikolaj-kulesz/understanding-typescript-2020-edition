type NumberOrString = number | string
type AsType = 'as-number' | 'as-string'

const combine = (
    a: NumberOrString,
    b: NumberOrString,
    type: AsType = 'as-number'
) => {
    if(type === 'as-number') return (+a + +b)
    return(a.toString() + b.toString())
}


console.log('combine', combine(5, 5));
console.log('combine', combine('abc', 'def'));
console.log('combine', combine('abc', 'def', 'as-string'));

