// function overloads
type Combinable = number | string

function myCombinableAdd(a: number, b: number): number;
function myCombinableAdd(a: string, b: number): string;
function myCombinableAdd(a: number, b: string): string;
function myCombinableAdd(a: string, b: string): string;
function myCombinableAdd (a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}

const t1 = myCombinableAdd(5,5);
const t2 = myCombinableAdd('a','b');
const t3 = myCombinableAdd(5,'b');
const t4 = myCombinableAdd('b',5);
t1.toString();
t2.split('');
t3.split('');
t4.split('');
