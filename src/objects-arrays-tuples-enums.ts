enum Positions {
    ADMIN,
    EDITOR,
    USER
}

interface Person {
    name: string;
    age: number;
    hobbies: string[];
    random: (string|number)[];
    role: [number, string];
    position: Positions,
}

const person: Person = {
    name: 'Mike',
    age: 55,
    hobbies: ['swimming', 'playing games', 'running'],
    random: [2, 'author', 4, 'bla bla'],
    role:  [2, 'author'],
    position: Positions.ADMIN,
}

console.log(person.name)
// console.log(person.random[1].toUpperCase()) // will not work since numbers and strings could be on random positions
console.log(person.role[1].toUpperCase())
console.log(person.position)

for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase())
}
