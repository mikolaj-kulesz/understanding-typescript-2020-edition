document.addEventListener("DOMContentLoaded", function (event) {
    new Class()
});


class Class {
    button: HTMLButtonElement | null;

    constructor() {
        this.button = document.querySelector('button');
        this.bindElements();
    }


    bindElements() {
        if (this.button) {
            this.button.addEventListener('click', this.clickHandler.bind(null, 'testMessage'))
        }
    }

    clickHandler(message: string) {
        console.log('message', message);
    }
}

