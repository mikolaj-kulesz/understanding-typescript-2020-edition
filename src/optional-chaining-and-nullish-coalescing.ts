// optional chaining

console.log('test');

const fetchedUserData = {
    name: "max",
    age: 10,
    // job: {
    //    title: 'FE',
    //    experience: '10years'
    // },
};

// @ts-ignore
console.log(fetchedUserData?.job?.title);

// ==================================================
// Nullish Coalescing
const someCustomUserInput = null;
// @ts-ignore
const storedData = someCustomUserInput ?? 'UNDEFINED';

console.log('storedData', storedData);