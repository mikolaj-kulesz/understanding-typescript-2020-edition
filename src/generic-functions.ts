// Generics Basics and Built-in Generics
const someArray1: string[] = ['one', 'two'];
const someArray2: Array<string> = ['one', 'two'];

const somePromise: Promise<number> = new Promise((resolve) => {
    setTimeout(() => {
        return resolve(10)
    }, 300)
});

somePromise.then(res => res.toString());

// Creating a generic function
const assign2Objects = <T,U>(a: T, b: U) => {
    return {
        ...a,
        ...b,
    }
};

const someResults1 = assign2Objects({name: 'skive', age: 34}, {hobbies: ['play', 'die']});
console.log('someResults1.age', someResults1.age);

const wrapObject = <T>(o: T, a: any[]) => {
    return [...a, o]
};

const someResults2 = wrapObject(someResults1, [1,2,3]);
console.log('someResults2', someResults2);

// Working with constrains
const wrapObject2 = <T, U extends any[]>(o: T, a: U) => {
    return [...a, o]
};

const someResults22 = wrapObject(someResults1, [{...someResults1}]);
console.log('someResults22', someResults22);

// Another generic function example
interface Lengthy {
    length: number
}

const countAndDescribe = <T extends Lengthy> (element: T) => {
    const {length} = element;
    let description = 'Got no elements.';
    if(length === 1) description = `Got one element.`;
    if(length > 1)  description = `Got ${length} elements.`;
    return [element, description]
};

console.log(countAndDescribe('Some test'));
console.log(countAndDescribe(['Some test', 'Some test']));
console.log(countAndDescribe([]));

// keyof example
const extractProperty = <T extends object, U extends keyof T>(object: T, propertyName: U) => {
    return `Value of ${propertyName} is: ${object[propertyName]}`
};

console.log(extractProperty({name: 'Miki'}, 'name'));
