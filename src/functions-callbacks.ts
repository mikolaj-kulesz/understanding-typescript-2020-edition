let customAdd = (a: number, b: number): number => {
    return a + b;
}

const addAndHandle = (a: number, b: number, cb: (r: number) => void): void => {
    const sum = a + b;
    cb(sum);
}

const printSomething = (someValue: number): void => {
    console.log(someValue);
}

let combineValues: (a: number, b: number) => number

combineValues = customAdd
// combineValues = printSomething // definition of that function is different, d
// combineValues = 5 // is not a function

console.log(customAdd(3, 5))
printSomething(5)

console.log(combineValues(4, 4));

addAndHandle(2, 2, (result) => {
    console.log('result', result);
})
