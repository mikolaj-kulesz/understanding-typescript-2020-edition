// Type casting
// !
const someEl1 = document.getElementById('some-element')! // with ! we ensure TS that element exist
console.log(someEl1.style)

// <>
const someEl2 = <HTMLInputElement>document.getElementById('some-element') // we ensure this way TS that we provide HTMLInputElement
console.log(someEl2.value)

// as
const someEl3 = document.getElementById('some-element') as HTMLInputElement // we ensure this way as well that TS that we provide HTMLInputElement
console.log(someEl3.value)

// if and as
const someEl4 = document.getElementById('some-element') // with existence checking
if (someEl4) {
    console.log((someEl4 as HTMLInputElement).value)
}

